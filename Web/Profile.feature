@update_profile
Feature: Update Profile
    Background: User is already on Homepage and click Profile button and go to Profile page
        Given user is already on Homepage
        When user click Profile button
        And user go to Profile page

    @positive_test
    Scenario: TC.Upd.Prof.001.001 - User can update profile
        When user update profile
        And user click Simpan button
        Then user successfully update profile

    @negative_test
    Scenario Outline: User can not update profile
    When user update profile but <conditions>
    And user click Simpan button
    Then user can see a pop-up message informing that <result>

        Examples:
            |       case_id        |          conditions            |             result                 |
            | TC.Upd.Prof.001.002  | leave Nama field empty         | Please fill out this field         |
            | TC.Upd.Prof.001.003  | without select Kota            | Please select an item in the list  |
            | TC.Upd.Prof.001.004  | leave Alamat field empty       | Please fill out this field         |
            | TC.Upd.Prof.001.005  | leave No Handphone field empty | Please fill out this field         |
            